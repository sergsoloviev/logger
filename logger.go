package logger

import (
	"log"
	"os"
)

func SetLogs(file string, output bool) {
	logfile, err := os.OpenFile(file, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		panic(err)
	}
	//defer logfile.Close()
	if output {
		log.SetOutput(logfile)
	}
}
